# Generated by Django 2.1.2 on 2018-10-11 20:14

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=100)),
                ('rut', models.CharField(max_length=10)),
                ('nombre', models.CharField(max_length=100)),
                ('fecha_nacimiento', models.DateField()),
                ('telefono', models.IntegerField()),
                ('region', models.CharField(max_length=20)),
                ('comuna', models.CharField(max_length=20)),
                ('Tipo_vivienda', models.CharField(max_length=20)),
            ],
        ),
    ]
