from django.shortcuts import render, redirect
from django.template import RequestContext
from .forms import UsuarioForm, SignUpForm, MascotaForm
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from .models import Mascota
from django.views.generic import ListView

def principal(request):
    model = Mascota.objects.all()
    contexto = {'mascota': model}
    return render(request, 'MisPerris/index.html', contexto)

def user_new(request):
    form = UsuarioForm()
    return render(request, 'MisPerris/registro.html', {'form': form})

            
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'MisPerris/registro.html', {'form': form})

@login_required(login_url='/accounts/login/')
def view_regmascota(request):
    form = MascotaForm(request.POST or None, request.FILES or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Mascota registrada!")
            return redirect('/mascota/new')

    else:
        form = MascotaForm()
    return render(request, 'MisPerris/regmascota.html', {'form': form})



class listmascota(ListView):
    model = Mascota
    template_name = 'MisPerris/listmascota.html'

class listadmin(ListView):
    model = Mascota
    template_name = 'MisPerris/listadmin.html'


def view_editmascota(request, id):
    mascota = Mascota.objects.get(id=id)
    if request.method=='GET':
        form = MascotaForm(instance=mascota)
    else:
        form = MascotaForm(request.POST, instance=mascota)
        if form.is_valid():
            form.save()
        return redirect('/mascota/list')
    return render(request, 'MisPerris/regmascota.html', {'form':form})


def mascota_delete(request, id):
    mascota = Mascota.objects.get(id=id)
    if request.method == 'POST':
        mascota.delete()
        return redirect('/mascota/admin')
        
    return render(request, 'MisPerris/deletemascota.html', {'mascota':mascota})
    
# Create your views here.
