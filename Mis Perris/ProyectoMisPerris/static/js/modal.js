$('.galeria_img').click(function(e){
    var img = e.target.src;
    $('body').append('<div class="modal" id="modal"> <img src="'+img+'" class="modal_img"> <div id="modal_boton" class="modal_boton">X</div></div>')
    $('#modal_boton').click(function(){
        $('#modal').remove();
    })
});

$(document).keyup(function(e){
    if(e.which==27){
        $('#modal').remove();
    }
});