from django.apps import AppConfig


class ProyectomisperrisConfig(AppConfig):
    name = 'ProyectoMisPerris'
