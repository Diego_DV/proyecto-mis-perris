from django import forms
from .models import Usuario, Mascota
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('email', 'rut', 'nombre', 'fecha_nacimiento', 'telefono', 'region', 'comuna', 'Tipo_vivienda',)

        labels = {
            'email': 'Correo electronico',
            'rut': 'Rut',
            'nombre' : 'Nombre completo',
            'fecha_nacimiento' : 'Fecha nacimiento',
            'telefono' : 'Telefono',
            'region' : 'Región',
            'comuna' : 'Comuna',
            'Tipo_vivienda' : 'Vivienda',
        }
        widgets= {
            'email': forms.TextInput,
            'rut': forms.TextInput,
            'nombre': forms.TextInput,

            
        } 


class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )
        help_texts = {
            'username': None,
            'email': None,
            'password1': None,
            'password2': None,
        }

CHOICES= (
('Rescatado', 'Rescatado'),
('Disponible', 'Disponible'),
('Adoptado', 'Adoptado'),
)

class MascotaForm(forms.ModelForm):
    class Meta:
        model = Mascota
        fields = ('fotografia', 'nombre', 'raza', 'descripcion', 'estado',)

        labels = {            
            'fotografia': 'Fotografia',
            'nombre' : 'Nombre',
            'raza' : 'Raza predominante',
        }

        widgets= {
            'descripcion': forms.Textarea(attrs={'class': 'row'}),
            'estado': forms.Select(choices=CHOICES),
        } 

