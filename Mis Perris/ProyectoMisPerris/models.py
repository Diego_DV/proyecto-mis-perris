from django.db import models
from django.utils import timezone
from django import forms

class Mascota(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=20)
    fotografia = models.ImageField(blank=True, null=False,
        upload_to="img")
    raza = models.CharField(max_length=25)
    descripcion = models.TextField()
    estado = models.CharField(max_length=60)

class Usuario(models.Model):
    email= models.EmailField()
    rut = models.CharField(max_length=10)
    nombre = models.CharField(max_length=100)
    fecha_nacimiento = models.DateField()
    telefono = models.IntegerField()
    region = models.CharField(max_length=20)
    comuna = models.CharField(max_length=20)
    Tipo_vivienda = models.CharField(max_length=20)


# Create your models here.
