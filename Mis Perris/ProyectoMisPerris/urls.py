from django.urls import path, include
from django.conf.urls import url
from .import views
from django.contrib.auth.views import LoginView, PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.conf import settings
from django.views.static import serve
from .views import listmascota
from .views import listadmin

urlpatterns = [
    path('', views.principal, name='principal'),
    path('Usuario/new', views.signup, name='user_new'),
    path('accounts/', include('django.contrib.auth.urls',)),
    path('mascota/new', views.view_regmascota, name='mascota_new'),
    path('mascota/list', listmascota.as_view(), name='mascota_list'),
    path('mascota/edit/<int:id>/' ,views.view_editmascota, name='mascota_edit'),
    path('mascota/admin', listadmin.as_view(), name='mascota_admin'),
    path('mascota/eliminar/<int:id>/', views.mascota_delete, name='mascota_delete'),
    path('reset/password_reset', PasswordResetView.as_view(), {'template_name':'reset/password_form.html', 'email_template_name':'reset/passowrd_email.html'}, name='password_reset'),
    path('reset/password_done', PasswordResetDoneView.as_view(), {'template_name':'reset/password_done.html'}, name='password_done'),
    path('reset/(?P<uidb64>[0-94-Za-z_\-]+)/(?P<token>.+)/$', PasswordResetConfirmView.as_view(), {'template_name':'reset/password_confirm.html'}, name='password_confirm' ),
    path('reset/complete', PasswordResetCompleteView.as_view(),{'template_name':'reset/password_complete.html'}, name='password_complete'),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$',
        serve, { 'document_root':
        settings.MEDIA_ROOT, }), ]